variable "environment" {
    description = "Environment of the IAC"
    type        = string
}

variable "key_pair" {
    description = "Name of the keypair"
    type        = string
}